
//========================================================================//
/*modules and parameterized routes.
	separates the ff.
1 Models - schema and models
2 Controllers - business logic like tasks and functions ang processes
3 Routes - endpoint														  */
//========================================================================//
/*npm init –y						to initialize the JS.
  npm install express mongoose	    to install express and mongoose
  nodemon fileName
  npx kill-port 4000 etc.
  ctrl + client															  */
//========================================================================//

		//////////////THE BASE/////////////////
	// //1 SETUP the dependencies or packages installed.
	// 	const express = require("express");
	// 	const mongoose = require("mongoose");
	// //2 setup server
	// 	const app = express();
	// //3 setup port
	// 	const port = 3000;
	// //4 for request later
	// 	app.use(express.json());
	// //5 database connection
	// 	mongoose.connect("mongodb+srv://admin:admin123@batch204.nanl81q.mongodb.net/B204-to-dos?retryWrites=true&w=majority",
	// 		{
	// 			useNewUrlParser: true,
	// 			useUnifiedTopology: true
	// 		}
	// 	);
	// //6 setup to see if the connection is ok and there is no error
	// 	let db = mongoose.connection;
	// 	db.on("error", console.error.bind(console, "Connection Error"));				
	// 	db.once("open", () => console.log("We are connected to the cloud database."))		
	// //7 listen
	// 	app.listen(port, () => console.log(`Now listening to port ${port}`))

//FIRST CREATE FOLDERS FOR "Modules" - "Routes" - "Controllers"
////////////////////////////////////////////////////////////////////////////////////////////////
//1// SETUP the dependencies or packages installed.
const express = require("express");
const mongoose = require("mongoose");
	///==added, this allows us to use all the routes defined///added from original base==///
const taskRoute = require("./routes/taskRoute")
//2// setup server
const app = express();
//3// setup port
const port = 3000;
//4// for request later
app.use(express.json());
//5// database connection
mongoose.connect("mongodb+srv://admin:admin123@batch204.nanl81q.mongodb.net/B204-to-dos?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);
//6// setup to see if the connection is ok and there is no error
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));				
db.once("open", () => console.log("We are connected to the cloud database."))		
//7 listen
	///==added task route endpoint//added from original base==///
app.use("/tasks", taskRoute)		//accessing 'task' collection in POSTMAN localhost:3000/tasks/
app.listen(port, () => console.log(`Now listening to port ${port}`))
////////////////////////////////////////////////////////////////////////////////////////// 














