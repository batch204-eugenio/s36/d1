//controllers contain the functions and business logic of our Express JS application
//we will import MODEL first to be able to use the methods like FIND.

//////////////THE BASE/////////////////
//IMPORT MODEL from MODEL FOLDER and subfolder Task so we can use its contents.
// const Task = require("../models/Task")	

// //create controller function for getting all the tasks
// //also EXPORT
// module.exports.getAllTasks = () => {		
// 	return Task.find({}).then(result => {	//return the result of our mongoose method which is FIND.
// 		return result						//({}) no criteria set, this means we will return all the tasks in our data collection
// 	})										//Result is all that we will find.
// }											//.then will get the result and then will return again to our client from the "result" we requested from POSTMAN.
// 											//.then is like waiting for .find to provide results it will
////////////////////////////////////////////////////////////////////////////////////////// 

//controllers contain the functions and business logic of our Express JS application
//we will import MODEL first to be able to use the methods like FIND.

//IMPORT MODEL from MODEL FOLDER and subfolder Task so we can use its contents.
const Task = require("../models/Task")	

//create controller function for getting all the tasks
//also EXPORT
///===added GET related to line 27-to 30 of taskRoute.js	
module.exports.getAllTasks = () => {		//sa .save 'error' muna bago "task"//sa .then "task" muna bago "error"	sa parameter	
	return Task.find({}).then(result => {	//return the result of our mongoose method which is FIND.
		return result						//({}) no criteria set, this means we will return all the tasks in our data collection
	})										//Result is all that we will find.
}											//.then will get the result and then will return again to our client from the "result" we requested from POSTMAN.
											//.then is like waiting for .find to provide results it will find.
///===added POST related to line 37-40 of taskRoute.js											
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {	//.save to save to mongoDB db//.then error will be the 2nd parameter
		if (error) {								//sa .save 'error' muna bago "task"//sa .then "task" muna bago "error" sa parameter
			console.log(error)	//error to terminal if true there is an error
			return false 		//false to postman if true there is an error
		} else {
			return task
		}
	})
}	
///===added DELETE related to line 43 to 50 of taskRoute.js
module.exports.deleteTask = (taskId) => {			//taskId is just a parameter user defined
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{	//Task.findByIdAndRemove() is built in.
		if (err) {														//mongoose methods -search id and then remove or delete the documents in our database
			console.log(err);	//error to terminal if true there is an error
			return false 		//false to postman if true there is an error
		} else {
			return removedTask
		}
	})
}										

///===added PUT related to line 51 to 57 of taskRoute.js
//in this example we updated what we posted, we updated the name: from "morning walk" to "Wake Up!"
//we used ID of what to be updated.
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error)	//error to terminal if true there is an error
			return false 		//false to postman if true there is an error
		}
		result.name = newContent.name;
		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				return false 	//false to postman if true there is an error
			} else {
				return updateTask
			}

		})
	})
}