//////////////THE BASE/////////////////
//containes all the endpoints for our application.
//we separate the routes such that "index.js file" would only contain the info on the server. 

////This is the basic////
// const express = require("express");
// //express.Router will allow us to use the routes here in our application.
// const router = express.Router();

// //Routes
// //Route to get all the tasks/docs in our collection
// router.get("/", (req, res) => {
// 	taskController.getAllTasks()					//taskController is user defined variable//getAllTasks() is defined.
// });
// To export the router object to use in the index.js file or fileName of our choice.
// module.exports = router;
//////////////////////////////////////////////////////////////////////////////////////////    


///same as above, we only added ../controllers/taskController path.///
///we also added .then(resultFromController => res.send(resultFromController))	
const express = require("express");
//express.Router will allow us to use the routes here in our application.
const router = express.Router();
	///==added after base==///
const taskController =require("../controllers/taskController")
//Routes
//Route to get all the tasks/docs in our collection
//GET method
router.get("/", (req, res) => {
								///==added after base==///

	taskController.getAllTasks().then(resultFromController => res.send(	//resultFromController is user defined.
		resultFromController))											//taskController is user defined variable//getAllTasks() is defined.
});
//let us create another task and method 
//POST method
router.post("/", (req, res) => {
	console.log(req.body)					//just for us to see what is inside the request body from POSTMAN body after we send.
	taskController.createTask(req.body).then(resultFromController => res.send(	//resultFromController is user defined.
		resultFromController))											
});
//DELETE method
//we will not use request body here.
//but req.params from /:id      //this /:id will be from mongoDB e.g //_id ObjectId('633c233c5e702eaf2779ce92'), 
router.delete("/:id", (req, res) => {						//just get the number and add it to localhost: on postman e.g. localhost:3000/tasks/633d78832dd17e602e458de3
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(    //req.params.id//.id is dot notation because we will access the object id.
		resultFromController));
});
//PUT/PATCH method UPDATE
//in this example we updated what we posted, we updated the name: from "morning walk" to "Wake Up!"
//we used ID of what to be updated.
router.put("/:id", (req, res) => {
	console.log(req.params.id)
	console.log(req.params)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(
		resultFromController));
});
//To export the router object to use in the index.js file or fileName of our choice.
module.exports = router;